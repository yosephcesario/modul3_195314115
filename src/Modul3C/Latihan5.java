/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3C;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

/**
 *
 * @author HP
 */
public class Latihan5  extends JDialog {

    private final JButton left;
    private final JButton right;
    private final JCheckBox centered,bold, italic ;
    private final JRadioButton red, green, blue;
    private final JTextArea text;
    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGTH = 400;
    private static final int BUTTON_WIDTH = 90;
    private static final int BUTTON_HEIGTH = 40;

    public Latihan5() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGTH);
        setResizable(true);
        setTitle("CheckBoxDemo");
        contentPane.setLayout(null);
        contentPane.setBackground(Color.GRAY);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        text = new JTextArea("Welcome to Java");
        text.setBounds(180, 50, 105, 100);
        contentPane.add(text);
        
        left = new JButton("Left");
        left.setBounds(160, 200, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(left);
        
        right = new JButton("Right");
        right.setBounds(260, 200, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(right);
        
        centered = new JCheckBox("Centreed");
        centered.setBounds(300, 50, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(centered);
        
        bold = new JCheckBox("Bold");
        bold.setBounds(300, 75, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(bold);
        
        italic = new JCheckBox("Italic");
        italic.setBounds(300, 100, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(italic);
        
        red = new JRadioButton("RED");
        red.setBounds(50, 50, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(red);
        
        blue = new JRadioButton("BLUE");
        blue.setBounds(50, 75, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(blue);
        
        green = new JRadioButton("GREEN");
        green.setBounds(50, 100, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(green);
    }

    public static void main(String[] args) {
        Latihan5 dialog = new Latihan5();
        dialog.setVisible(true);
    }

}
    

