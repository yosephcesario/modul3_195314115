/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3C;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JDialog;

/**
 *
 * @author HP
 */
public class Latihan2 extends JDialog {
    
    private final JButton button1;
    private final JButton button2;
    private final JButton button3;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGTH = 30;
    
    public Latihan2(){
        Container contentPane = getContentPane();
        setSize(350, 250);
        setResizable(true);
        setTitle("Button Test");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        button1 = new JButton("Yellow");
        button1.setBackground(Color.YELLOW);
        button1.setBounds(30, 10, BUTTON_WIDTH, BUTTON_HEIGTH);       
        contentPane.add(button1);
        button2 = new JButton("Blue");
        button2.setBackground(Color.BLUE);
        button2.setBounds(130, 10,BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(button2);
        button3 = new JButton("Red");
        button3.setBackground(Color.RED);
        button3.setBounds(230, 10,BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(button3);
    }
    
    public static void main(String[] args) {
        Latihan2 dialog = new Latihan2();
        dialog.setVisible(true);
    }
}

    

