/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3C;

import java.awt.Container;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

/**
 *
 * @author HP
 */
public class Latihan3 extends JDialog {
        private final JLabel image;
    private final JLabel text;
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 300;
    public Latihan3(){
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Text and Icon Label");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        text = new JLabel("GRAPE");
        text.setBounds(35, 100, 97, 197);
        contentPane.add(text);
        ImageIcon imageic = new ImageIcon(("download.png"));
        image = new JLabel();
        image.setIcon(imageic);
        image.setBounds(5, 10, 100, 100);
        this.add(image);
    }
    public static void main(String[] args) {
        Latihan3 dialog = new Latihan3();
        dialog.setVisible(true);
}


}
    

