/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3C;

import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

/**
 *
 * @author HP
 */
public class Latihan6 extends JDialog {

    private String str1;
    private JTextArea area1;
    private JScrollPane scrollPane1;
    private JPanel panel;

    public Latihan6() {
        initTextAreaScrol();
        initGambarComboBox1();
        setTitle("ComboBoxDemo");
        setSize(500, 300);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    private void initGambarComboBox1() {

        String[] negara = {"Indonesia", "English", "Canada", "Malaysia"};
        this.setLayout(null);
        JComboBox comboBox_negara = new JComboBox(negara);
        comboBox_negara.setBounds(1, 1, 400, 20);
        this.add(comboBox_negara);

        URL indonesia = this.getClass().getResource("indonesia.png");
        ImageIcon imageIndonesia = new ImageIcon(indonesia);
        JLabel labelGambar = new JLabel(imageIndonesia);
        labelGambar.setBounds(2, 10, 250, 200);
        this.add(labelGambar);
        JLabel labelNamaGambar2 = new JLabel("Indonesia");
        labelNamaGambar2.setBounds(2, 10, 250, 200);
        this.add(labelGambar);

    }

    private void initTextAreaScrol() {

        str1 = "indonesia disebut juga dengan Republik Indonesia (RI)"
                + " atau Negara Kesatuan Republik Indonesia (NKRI), adalah negara di Asia Tenggara yang dilintasi "
                + "garis khatulistiwa dan berada di antara daratan benua Asia dan Australia, serta antara "
                + "Samudra Pasifik dan Samudra Hindia. Indonesia adalah negara kepulauan terbesar di dunia"
                + " yang terdiri dari 17.504 pulau.[14] Nama alternatif yang biasa dipakai adalah Nusantara. "
                + "Dengan populasi Hampir 270.054.853 jiwa pada tahun 2018, Indonesia adalah negara berpenduduk "
                + "terbesar keempat di dunia dan negara yang berpenduduk Muslim terbesar di dunia, dengan lebih"
                + " dari 230 juta jiwa.";

        area1 = new JTextArea(str1);
        area1.setLineWrap(true);
        area1.setWrapStyleWord(true);
        area1.setBounds(300, 40, 160, 200);
        scrollPane1 = new JScrollPane(area1, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane1.setBorder(new TitledBorder("Bendera Indonesia"));
        scrollPane1.setBounds(300, 40, 160, 200);
        this.add(scrollPane1);
    }

    public static void main(String[] args) {
        Latihan6 test = new Latihan6();
        test.setVisible(true);
        test.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
