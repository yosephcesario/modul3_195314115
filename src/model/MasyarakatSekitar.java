package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HP
 */
public class MasyarakatSekitar extends penduduk {
    private String nomor;
    
    MasyarakatSekitar(){       
    }
    public MasyarakatSekitar(String nim,String nama,String ttl){
        super(nama,ttl);           
    }
    public String getNomor() {
        return nomor;
    }
    public void setNomor(String datanomor) {
        this.nomor = datanomor;
    }      
    @Override
    double hitungIuran() {
     double iuran = Double.parseDouble(nomor);
     return iuran*100;
    }
    
}
