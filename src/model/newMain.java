package model;


import java.util.Scanner;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HP
 */
public class newMain {
        public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        UKM ukm = new UKM();
        ukm.setNamaUnit("UKM Teksapala");
        Mahasiswa ketua = new Mahasiswa();
        ketua.setNama("Cesar");
        ketua.setNim("195314115");
        ketua.setTtl("Samarinda,19 Maret 2002");
        
        Mahasiswa sekretaris = new Mahasiswa();
        sekretaris.setNama("Fika");
        sekretaris.setNim("191314039");
        sekretaris.setTtl("Bogor,24 februari 1998");
        
        ukm.setKetua(ketua);
        ukm.setSekertaris(sekretaris);
        
        System.out.println("<< " + ukm.getNamaUnit()+" >>");
        System.out.println("==============================");
        System.out.println("Ketua     \t: " + ukm.getKetua().getNama());
        System.out.println("Nim       \t: " + ukm.getKetua().getNim());
        System.out.println("TTL       \t: " + ukm.getKetua().getTtl());
        System.out.println("Sekretaris\t: " + ukm.getSekertaris().getNama());
        System.out.println("Nim       \t: " + ukm.getSekertaris().getNim());
        System.out.println("TTL       \t: " + ukm.getSekertaris().getTtl());
        System.out.println("===========================================================");
        System.out.println("     Anggota atau Mahasiswa : ");
        System.out.println("No. Nama\t Nomor\t\t TTL\t\t Iuran");
        int n = Integer.parseInt(JOptionPane.showInputDialog(null, "Masukkan jumlah anggota Mahasiswa : "));
        Mahasiswa[] mhs = new Mahasiswa[n];
        for (int i = 0; i < n; i++) {
            mhs[i] = new Mahasiswa();
            String nama = JOptionPane.showInputDialog(null, "Masukkan nama mahasiswa : ");
            String nim = JOptionPane.showInputDialog(null, "Masukkan NIM : ");
            String ttl = JOptionPane.showInputDialog(null, "Masukkan Tempat tanggal lahir : ");
            mhs[i].setNama(nama);
            mhs[i].setNim(nim);
            mhs[i].setTtl(ttl);

            System.out.println(+(i + 1)+". "+ mhs[i].getNama() + "\t\t" + mhs[i].getNim()
                    + "\t" + mhs[i].getTtl() + "\t" + mhs[i].hitungIuran());
            System.out.println("=================================================================");
        }

        int m = Integer.parseInt(JOptionPane.showInputDialog(null, "Masukkan jumlah anggota Masyarakat : "));
        MasyarakatSekitar[] ms = new MasyarakatSekitar[m];
        for (int j = 0; j < m; j++) {
            ms[j] = new MasyarakatSekitar();
            String nama = JOptionPane.showInputDialog(null, "Masukkan nama anggota baru : ");
            String nik = JOptionPane.showInputDialog(null, "Masukkan nomor/nim : ");
            String ttl = JOptionPane.showInputDialog(null, "Masukkan Tempat tanggal lahir : ");
            ms[j].setNama(nama);
            ms[j].setNomor(nik);
            ms[j].setTtl(ttl);

            System.out.println(" Anggota baru");
            System.out.println(+(j + 1) + ". " + ms[j].getNama() + "\t" + ms[j].getNomor()
                    + "\t" + ms[j].getTtl() + "\t" + ms[j].hitungIuran());
            System.out.println("=================================================================");
            
        }

    }
}
