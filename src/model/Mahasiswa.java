package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HP
 */
public class Mahasiswa extends penduduk{
    private String nim;
    
    public Mahasiswa(){      
    }
    public Mahasiswa(String nim,String nama,String ttl){
        super(nama,ttl);
        this.nim = nim;      
    }
    public String getNim() {
        return nim;
    }
    public void setNim(String nim) {
        this.nim = nim;
    }
    @Override
    double hitungIuran() {
        double iuran = Double.parseDouble(getNim());
        return iuran/10000;
}}
