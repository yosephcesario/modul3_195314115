package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HP
 */
public abstract class penduduk implements NewInterface {

    private String nama;
    private String ttl;

    public penduduk() {

    }
    public penduduk(String nama, String ttl) {
        this.nama = nama;
        this.ttl = ttl;
    }
    @Override
    public String getNama() {
        return nama;
    }
    @Override
    public void setNama(String nama) {
        this.nama = nama;
    }
    @Override
    public String getTtl() {
        return ttl;
    }
    @Override
    public void setTtl(String ttl) {
        this.ttl = ttl;
    }
    abstract double hitungIuran();

}
