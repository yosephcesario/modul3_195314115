/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3A;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author HP
 */
public class Latihan4A extends JFrame {

    public Latihan4A() {
        this.setSize(600, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("find");
        this.setVisible(true);
        this.setLayout(null);

        JLabel label = new JLabel("                keyword: ");
        label.setBounds(150, 20, 150, 20);
        this.add(label);

        JTextField text = new JTextField();
        text.setBounds(150, 40, 150, 20);
        this.add(text);

        JButton tombol = new JButton();
        tombol.setBounds(150, 60, 150, 20);
        tombol.setText("find");
        this.add(tombol);

    }

    public static void main(String[] args) {
        new Latihan4A();
    }

}
