/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3A;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author Daffa
 */
public class Latihan3A extends JFrame {

    public Latihan3A() {
        this.setLayout(null);
        this.setSize(500, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("ini class turunan dari jframe");
        this.setVisible(true);

        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("ini tombol");
        panel.add(tombol);
        this.add(panel);

        JLabel label = new JLabel("Label");
        label.setBounds(12, 12, 200, 20);
        this.add(label);


        JTextField teks = new JTextField("TextField");
        teks.setBounds(12, 32, 200, 20);
        this.add(teks);


        JCheckBox cekBox = new JCheckBox("Checkbox");
        cekBox.setBounds(12, 52, 200, 20);
        this.add(cekBox);
        

        JRadioButton radiobtn = new JRadioButton("Radio");
        radiobtn.setBounds(12, 72, 200, 20);
        this.add(radiobtn);
    }

    public static void main(String[] args) {
        new Latihan3A();
    }
}